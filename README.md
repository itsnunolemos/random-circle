# Random Circle

[See the live demo](https://random-circle.netlify.com/)

## Developing
This project use:
- [Vue.js](https://vuejs.org/)
- [BEEM CSS Methodology](http://getbem.com/introduction/)
- [SCSS](https://sass-lang.com/)
- [GitHub flow](https://guides.github.com/introduction/flow/)
- [GitLab CI](https://docs.gitlab.com/ee/ci/)
- [ESLint](https://eslint.org/)
- [Jest](https://jestjs.io/)
- [Netlify](https://www.netlify.com/)

## Getting Started
These instructions will get you a copy of the project up and running on your local and production machine.

### Requirements

| name   | >=     |
| :----- | -----: |
| `node` | `8.9` |
| `npm`  | `5.5.1`  |

### Running in development

```bash
# Install dependencies
npm install

# Compiles and hot-reloads for development
npm run serve
```

Open [http://localhost:8080](http://localhost:8080) to view it in the browser.


### Running unit tests and fixes files

```bash
# Run your unit tests
npm run test:unit

# Lints and fixes files
npm run lint
```

### Running in production

```bash
# Compiles and minifies for production
npm run build
```

Builds the app for production to the `dist` folder.
