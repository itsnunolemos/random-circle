import { mount } from '@vue/test-utils'
import ConsoleBoard from '@/components/ConsoleBoard'

describe('ConsoleBoard.vue', () => {
  it('renders props.consoleInfo when passed', () => {
    const consoleInfo = [{
      foo: 'foo'
    },
    {
      bar: 'bar'
    }]
    const wrapper = mount(ConsoleBoard, {
      propsData: {
        consoleInfo
      }
    })
    expect(wrapper.props().consoleInfo).toBe(consoleInfo)
  })
})
